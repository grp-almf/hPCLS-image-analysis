# General description

Jython script for Fiji for processing and quantifying collagen deposition on the microscopy images of high precision cut lung slices (hPCLS). 

# Contributors
- Muzamil Majid Khan (Pepperkok Lab): project runner, main user and tester
- Christian Tischer (CBA): initial development
- Aliaksandr Halavatyi (ALMF): development and support

# Dependencies:
- Fiji
- AutoMicTools library

See [Documentation](./Documentation/hPCLS_analysis_Docu.md) for the guidelines

# Documentation

Under this [link](./Documentation/hPCLS_analysis_Docu.md) you will find the detailed description of the tool functions and step-by-step guidelines for its use.

# Citation

Please cite following work when using this tool:

> Khan MM, Poeckel D, Halavatyi A, et al. An integrated multiomic and quantitative label-free microscopy-based approach to study pro-fibrotic signalling in ex vivo human precision-cut lung slices. Eur Respir J 2020; in press
(https://doi.org/10.1183/13993003.00221-2020).


# Contact

Please contact Muzamil Majid Khan ([muzamil.m.khan@embl.de](muzamil.m.khan@embl.de)) and Aliaksandr Halavatyi ([aliaksandr.halavatyi@embl.de](aliaksandr.halavatyi@embl.de)) if you have questions, comments or suggestions about using this tool.
