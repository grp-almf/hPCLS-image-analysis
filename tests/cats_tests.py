# testing CATS plugins from Christian Tischer
# Aim: include to 3D-tissue-analysis-grid script
# Date:2018-12-19 
# Modified: 2018-12-26


#imports
from de.embl.cba.cats import CATS
from de.embl.cba.cats.results import ResultExportSettings
#from de.embl.cba.cats.results import ResultImage

from ij import IJ
from ij import ImageStack
from ij import ImagePlus

#settings
inputImagePath = r"D:\tempDat\Muzamil\test_crop_machine_learning\input-to-classify.tif"
classifierPath = r"D:\tempDat\Muzamil\test_crop_machine_learning\classifier.classifier"
outputDirectory = r"D:\tempDat\Muzamil\test_crop_machine_learning\classifierOutput"
outputFileNamesPrefix = "predict"

img=IJ.openImage(inputImagePath)


cats=CATS()
cats.setInputImage(img)

cats.setResultImageRAM()

cats.loadClassifier(classifierPath)

cats.applyClassifierWithTiling()

'''
# export results to image files
## specify settings
resultExportSettings = ResultExportSettings()
resultExportSettings.inputImagePlus = cats.getInputImage()
resultExportSettings.exportType = ResultExportSettings.TIFF_STACKS
resultExportSettings.directory = outputDirectory
resultExportSettings.exportNamesPrefix = outputFileNamesPrefix + "-"
resultExportSettings.classNames = cats.getClassNames()

## save results
resultImage = cats.getResultImage();
resultImage.exportResults( resultExportSettings )
'''

# get results as ImagePlus in Fiji
resultExportSettings = ResultExportSettings()
resultExportSettings.exportType = ResultExportSettings.GET_AS_IMAGEPLUS_ARRAYLIST
resultExportSettings.classNames = cats.getClassNames()
resultExportSettings.directory = outputDirectory
resultExportSettings.inputImagePlus = cats.getInputImage()
resultExportSettings.resultImage = cats.getResultImage()
resultExportSettings.timePointsFirstLast = [0, 0]

resultImage = cats.getResultImage()
classImages=resultImage.exportResults(resultExportSettings)

print type(classImages[0])
#print resultImage


#for image in classImages:
#    image.show()
## forming stack of images
classNames=cats.getClassNames()
classStack=ImageStack(classImages[0].getWidth(),classImages[0].getHeight())
for classIndex in range(0, classNames.__len__()):
    classStack.addSlice(classNames[classIndex],classImages[classIndex].getProcessor())

allClassImage=ImagePlus("All Class Image", classStack)
allClassImage.show()

print 'Done :)'