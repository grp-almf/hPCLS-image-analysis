from ij.io import OpenDialog
from ij.gui import GenericDialog, WaitForUserDialog
from ij.plugin import ZProjector, RGBStackMerge, SubstackMaker, Duplicator,ImageCalculator
from ij import IJ, ImagePlus, ImageStack, WindowManager,Prefs
from ij.process import StackStatistics,ImageConverter
from ij.plugin.frame import RoiManager
from imagescience.feature import Hessian
from imagescience.image import Image,FloatImage
import os, os.path, re, sys
import shutil
from mcib3d.image3d import Segment3DImage

from loci.plugins import BF
from loci.common import Region
from loci.plugins.in import ImporterOptions

from automic.table import TableModel            # this class stores the data for the table
from automic.table import ManualControlFrame    #this class visualises TableModel via GUI
from automic.utils.roi import ROIManipulator3D

initialNucleiThreshold=(1.5,7.0)

def close_all_image_windows():
  # forcefully closes all open images windows
  ids = WindowManager.getIDList();
  if (ids==None):
    return
  for i in ids:
     imp = WindowManager.getImage(i)
     if (imp!=None):
       win = imp.getWindow()
       if (win!=None):
         imp.changes = False # avoids the "save changes" dialog
         win.close()
         

def extractChannel(imp, nChannel, nFrame):
  """ Extract a stack for a specific color channel and time frame """
  stack = imp.getImageStack()
  ch = ImageStack(imp.width, imp.height)
  for i in range(1, imp.getNSlices() + 1):
    index = imp.getStackIndex(nChannel, i, nFrame)
    ch.addSlice(str(i), stack.getProcessor(index))
  return ImagePlus("Channel " + str(nChannel), ch)

def measureSumIntensity3D(imp):
  stats = StackStatistics(imp)
  return stats.mean * stats.pixelCount

def threshold(_img, lower_threshold, upper_threshold):
  imp = Duplicator().run(_img)
  #imp.show(); time.sleep(0.2)
  #IJ.setThreshold(imp, mpar['lthr'], mpar['uthr'])
  IJ.setThreshold(imp, lower_threshold, upper_threshold)
  IJ.run(imp, "Convert to Mask", "stack")
  imp.setTitle("Threshold");
  #IJ.run(imp, "Divide...", "value=255 stack");
  #IJ.setMinAndMax(imp, 0, 1);
  return imp


def processNucleiImage(_nucleiImage, _parameters):
  _nucleiImage.show()
  hessian=Hessian()
  eigenImages=hessian.run(FloatImage(Image.wrap(_nucleiImage)),_parameters['Hessian Smoothing Scale'],True)
  smallestEigenImage=eigenImages.get(2).imageplus()
  smallestEigenImage.show()

  gaus1Image = smallestEigenImage.duplicate();
  IJ.run(gaus1Image, "Gaussian Blur...", "sigma=%f stack" %p['Gaussian radius 1'])

  gaus2Image = smallestEigenImage.duplicate();
  IJ.run(gaus2Image, "Gaussian Blur...", "sigma=%f stack" %p['Gaussian radius 2'])

  imageClean=ImageCalculator().run("Subtract create 32-bit stack", gaus1Image, gaus2Image)
  imageClean.show()
  IJ.run('Threshold...')
  IJ.setThreshold(imageClean,initialNucleiThreshold[0],initialNucleiThreshold[1])
  WaitForUserDialog('Adjust threshold manulally').show()
  firstProcessor=imageClean.getProcessor()
  thresholds=(firstProcessor.getMinThreshold(),firstProcessor.getMaxThreshold())

  IJ.run(imageClean, "Options...", "iterations=1 count=1")

  IJ.run(imageClean, "Convert to Mask", "stack")

  segmentor3D=Segment3DImage(imageClean.duplicate(),125,255)
  segmentor3D.setMinSizeObject(p['Min nucleus size'])
  segmentor3D.setMaxSizeObject(p['Max nucleus size'])
  segmentor3D.segment()
  segmentedRois=segmentor3D.getLabelledObjectsImage3D().getObjects3DPopulation()


  return imageClean, segmentedRois,thresholds

def summarise3DRois(_3Dpopulation):


  if _3Dpopulation is not None:
    nNuclei= _3Dpopulation.getNbObjects()
  else:
    nNuclei=0

  
  totalVolume=0
  if nNuclei>0:
    print _3Dpopulation
    for iNucleus in  range(0,nNuclei):
      totalVolume=totalVolume+_3Dpopulation.getObject(iNucleus).getVolumePixels()

  return nNuclei, totalVolume


def analyze(_iDataSet, _tbModel, _p, _output_folder):

  #
  # LOAD FILES
  #

  filepath = tbModel.getFileAbsolutePathString(_iDataSet, "RAW", "IMG")
  filename = tbModel.getFileName(_iDataSet, "RAW", "IMG")
  
  print("Analyzing: "+filepath)

  originalImage=BF.openImagePlus(filepath)[0]
  if originalImage.getNChannels()<2:
    raise Exception('Incompatible number of channels in the dataset %d' %(iDataset+1))

  
  IJ.run("Options...", "iterations=1 count=1"); 

  #analyze nuclei
  nuclei_Image=extractChannel(originalImage, p['Nuclei Channel Index'], 1)
  nuclei_Mask, nuclei_ROIs,nucleiHessianThresholds=processNucleiImage(nuclei_Image,p)


  output_filename = 'NuclearMask--%s.tif' %filename
  tbModel.setFileAbsolutePath(output_folder, output_filename, _iDataSet, "Nuclei_Mask", "IMG")
  IJ.saveAs(nuclei_Mask, "Tiff", os.path.join(output_folder,output_filename))

  nNuclei,totalVolume=summarise3DRois(nuclei_ROIs)

  tbModel.setNumericValue(nNuclei, _iDataSet, 'Nuclei_Count')
  tbModel.setNumericValue(totalVolume, _iDataSet, 'Nuclei_Total_Volume')
  if nNuclei>0:
    ROIManipulator3D.populationToFile(output_folder,'Nuclei-3D-ROIs--%s' %filename,nuclei_ROIs)
    tbModel.setFileAbsolutePath(output_folder, 'Nuclei-3D-ROIs--%s.zip' %filename, _iDataSet, "Nuclei_ROIs", "ROI")

  tbModel.setNumericValue(nucleiHessianThresholds[0], _iDataSet, "Nuclei_Hessian_Lower_Threshold")
  tbModel.setNumericValue(nucleiHessianThresholds[1], _iDataSet, "Nuclei_Hessian_Upper_Threshold")


  #analyze calcein
  calcein_channel_index=p['Calcein Channel Index']
  lower_threshold_value = p['Calcein Lower Threshold']
  upper_threshold_value = p['Calcein Upper Threshold']
  print("gating Calcein channel (#%d) with values from %d to %d" %(calcein_channel_index,lower_threshold_value,upper_threshold_value))
  calcein_Image=extractChannel(originalImage, calcein_channel_index, 1)
  bw_image = threshold(calcein_Image, lower_threshold_value, upper_threshold_value)

  output_filename = 'BW-Calcein--%s.tif' %filename
  tbModel.setFileAbsolutePath(output_folder, output_filename, _iDataSet, 'BW_Calcein',"IMG")
  IJ.saveAs(bw_image, "Tiff", os.path.join(output_folder,output_filename))

  # convert from 0,255 to 0,1 for the following calculations
  image_01 = bw_image.duplicate()
  IJ.run(image_01, "Divide...", "value=255 stack"); 

  # set all pixels in the intensity image to zero that are not within the gate
  gated_calcein_image = ImageCalculator().run("Multiply create stack", calcein_Image, image_01)

  # save gated image
  output_filename = 'Gated-Calcein--%s.tif' %filename
  IJ.saveAs(gated_calcein_image, "Tiff", os.path.join(output_folder,output_filename))
  tbModel.setFileAbsolutePath(output_folder, output_filename, _iDataSet, 'GATED_Calcein', "IMG")

  # now measure in gated_calcein_image and image_01
  tbModel.setNumericValue(round(measureSumIntensity3D(image_01),0), _iDataSet, "PBT_Calcein")
  tbModel.setNumericValue(round(measureSumIntensity3D(gated_calcein_image),0), _iDataSet, "SumIntensity_Calcein")
  tbModel.setNumericValue(p['Calcein Lower Threshold'], _iDataSet, "Calcein_Lower_Threshold")
  tbModel.setNumericValue(p['Calcein Upper Threshold'], _iDataSet, "Calcein_Upper_Threshold")

#
# ANALYZE INPUT FILES
#
def determine_input_files(foldername, tbModel, _inputFileExtension):

  print("Determine input files in:",foldername)
  PBTtern = re.compile('(.*)'+_inputFileExtension) 
  
  #PBTtern = re.compile('(.*)--beats.tif') 
   
  i = 0
  for root, directories, filenames in os.walk(foldername):
    for filename in filenames:
       print("Checking:", filename)
       if (filename == "Thumbs.db") or filename.startswith('.'):
         continue
       match = re.search(PBTtern, filename)
       if (match == None) or (match.group(1) == None):
         continue
       tbModel.addRow()
       tbModel.setFileAbsolutePath(foldername, filename, i, "RAW","IMG")
       print("Accepted:", filename)
       
       i += 1
    
  return(tbModel)

#
# GET PARAMETERS
#
def get_parameters(num_data_sets):
  gd = GenericDialog("Please enter parameters")

  gd.addMessage("Found "+str(num_data_sets)+" data sets")
  gd.addStringField("analyse", "all")

  gd.addMessage("Image analysis parameters:")
  gd.addMessage("Please note: the threshold values are inclusive!\nThus, to exlude pixels with value 255 the upper threshold needs to be 254")

  gd.addMessage('Nuclei Analysis specification')
  gd.addNumericField('Nuclei Channel Index', 1, 0)
  gd.addNumericField('Hessian Smoothing Scale', 1.0, 2)
  gd.addNumericField('Gaussian radius 1', 3.0, 2)
  gd.addNumericField('Gaussian radius 2', 5.0, 2)
  gd.addNumericField('Min nucleus size', 5, 0)
  gd.addNumericField('Max nucleus size', 5000, 0)
  
  gd.addMessage('Calcein Analysis specification')
  gd.addNumericField('Calcein Channel Index', 2, 0)
  gd.addNumericField('Calcein Lower Threshold', 30.0, 2)
  gd.addNumericField('Calcein Upper Threshold', 255.0, 2)

  gd.showDialog()
  if gd.wasCanceled():
    return

  p = dict()
  to_be_analyzed = gd.getNextString()

  p['Nuclei Channel Index'] = int(gd.getNextNumber())
  p['Hessian Smoothing Scale'] = gd.getNextNumber()
  p['Gaussian radius 1'] = gd.getNextNumber()
  p['Gaussian radius 2'] = gd.getNextNumber()
  p['Min nucleus size'] = int(gd.getNextNumber())
  p['Max nucleus size'] = int(gd.getNextNumber())
  
  p['Calcein Channel Index'] = int(gd.getNextNumber())
  p['Calcein Lower Threshold'] = gd.getNextNumber()
  p['Calcein Upper Threshold'] = gd.getNextNumber()

  
  return to_be_analyzed, p


if __name__ == '__main__':

  print("")
  print("#")
  print("# Tissue analysis with nuclei")
  print("#")
  print("")
  
  #
  # GET INPUT FOLDER
  #
  od = OpenDialog("Click on one of the image files in the folder to be analysed", None)
  input_folder = od.getDirectory()
  if input_folder is None:
    sys.exit("No folder selected!")
  
  inputFileExtension=os.path.splitext(od.getFileName())[1]

  #
  # MAKE OUTPUT FOLDER
  #
  output_folder = input_folder[:-1]+"--fiji"
  if os.path.isdir(output_folder):
    shutil.rmtree(output_folder)
  os.mkdir(output_folder)

  #
  # DETERMINE INPUT FILES
  #
  tbModel = TableModel(output_folder)
  tbModel.addFileColumns('RAW','IMG')
  tbModel = determine_input_files(input_folder, tbModel, inputFileExtension)
  
  #
  # CHECK FIRST IMAGE
  #
  filepath = tbModel.getFileAbsolutePathString(0, "RAW", "IMG")
  print("Analyzing: "+filepath)
  settingsImage=BF.openImagePlus(filepath)[0]
  bit_depth = settingsImage.getBitDepth()
  
  
  #
  # GET PARAMETERS
  #
  to_be_analyzed, p= get_parameters(tbModel.getRowCount())

  #
  # POPULATE AND SHOW INTERACTIVE TABLE
  #

  # nuclei Analysis
  tbModel.addValueColumn("Nuclei_Count", "NUM")
  tbModel.addValueColumn("Nuclei_Total_Volume", "NUM")
  tbModel.addValueColumn("Nuclei_Hessian_Lower_Threshold", "NUM")
  tbModel.addValueColumn("Nuclei_Hessian_Upper_Threshold", "NUM")
  
  tbModel.addFileColumns('Nuclei_Mask','IMG')
  tbModel.addFileColumns('Nuclei_ROIs','ROI')


  # Calcein Analysis
  tbModel.addValueColumn("PBT_Calcein", "NUM")
  tbModel.addValueColumn("SumIntensity_Calcein", "NUM")
  tbModel.addValueColumn("Calcein_Lower_Threshold", "NUM")
  tbModel.addValueColumn("Calcein_Upper_Threshold", "NUM")



  # binary and gated output images
  tbModel.addFileColumns('BW_Calcein','IMG')
  tbModel.addFileColumns('GATED_Calcein','IMG')

#  tbModel.addFileColumns('Exclusion.Roi','ROI')

  frame=ManualControlFrame(tbModel,False)
  frame.setVisible(True)
  
  #initialise Roi manager
  #rm=RoiManager.getInstance()
  #if rm is None:
  #  rm=RoiManager()
  
  #
  # ANALYZE
  #

  if not to_be_analyzed=="all":
    close_all_image_windows()
    analyze(int(to_be_analyzed)-1, tbModel, p, output_folder)
  else:
    for i in range(tbModel.getRowCount()):
      close_all_image_windows()
      analyze(i, tbModel, p, output_folder)
#
  close_all_image_windows()

  tbModel.writeNewFile('analysis_summary.txt',True)

  print 'Analysis finished'