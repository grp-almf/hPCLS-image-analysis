dir=getDirectory("Choose a Directory"); 
//print(dir); 
//NuclearBinaryDir=dir + "\NuclearBinary\\"; 
//print(NuclearBinaryDir);
//File.makeDirectory(NuclearBinaryDir); 
list = getFileList(dir);
for (i=0; i<list.length; i++) { 
     if (endsWith(list[i], ".tif")){ 
               print(i + ": " + dir+list[i]); 
             open(dir+list[i]); 
             imgName=getTitle(); 
         

         selectWindow(imgName); 
         run("FeatureJ Hessian", " smallest absolute smoothing=1.0");
         run("Duplicate...", "duplicate");
         selectWindow(imgName + " smallest Hessian eigenvalues");
         run("Gaussian Blur...", "sigma=3 stack");
         selectWindow(imgName + " smallest Hessian eigenvalues-1");
         run("Gaussian Blur...", "sigma=5 stack");
         imageCalculator("Subtract create stack", imgName + " smallest Hessian eigenvalues", imgName + " smallest Hessian eigenvalues-1");
         selectWindow("Result of "+imgName+" smallest Hessian eigenvalues"); 
         run("Threshold...");    
  		 setThreshold(1.5,7);
         setOption("BlackBackground", false);
         run("Make Binary", "method=Default background=Default");
         saveAs("binary"+imgName);
         close();
         run("Close All");
     }
} 
         
         