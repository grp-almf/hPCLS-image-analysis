
from ij.io import OpenDialog
from ij.gui import GenericDialog
from ij.gui import NonBlockingGenericDialog,Roi,OvalRoi,WaitForUserDialog,ShapeRoi,Overlay
from ij.plugin import ZProjector, RGBStackMerge, SubstackMaker, Concatenator,ImageInfo
from ij.plugin import Duplicator, ImageCalculator
from ij import IJ, ImagePlus, ImageStack, WindowManager,Prefs
from ij.process import StackStatistics
from ij.process import ImageConverter
from ij.measure import ResultsTable
from ij.plugin.frame import RoiManager
from ij.plugin.filter import RankFilters, ThresholdToSelection
import os, os.path, re, sys
from jarray import array
import math
from math import sqrt

from loci.plugins import BF
from loci.common import Region
from loci.plugins.in import ImporterOptions

from automic.table import TableModel            # this class stores the data for the table
from automic.table import ManualControlFrame    #this class visualises TableModel via GUI
from java.io import File


inputFileExtention='czi'
initialMinMaxRadius=5

defDirectoryPrefsTag='tissue.analysis.default.directory'


def getCombinationsList(_nChannels):
    resultList=list()
    for f in range(1,_nChannels+1):
        for s in range(f+1,_nChannels+1):
            resultList.append((f,s))
    return resultList

def getImageSettingKeys(_nChannels):
    imageSettingKeys=['Experiment|AcquisitionBlock|Laser|LaserPower #1 = ','Information|Instrument|LightSource|Power #1 = ']
    
    imageSettingKeysShort=['LaserPower #1','Power #1']
    
    for iChannel in range(1,_nChannels+1):
        imageSettingKeys.append('Information|Image|Channel|Gain #%d = ' %iChannel)
        imageSettingKeysShort.append('Gain #%d' %iChannel)
    
    return imageSettingKeys,imageSettingKeysShort



def getImageSettings(inputImage,_imageSettingKeys):
    infoString=ImageInfo().getImageInfo(inputImage)
    
    infoInputs=infoString.split('\n')
    
    targetSettings={}
    
    for prefix in _imageSettingKeys:
        targetSettings[prefix]=float('nan')
    
    for infoInput in infoInputs:
        for prefix in _imageSettingKeys:
            if (infoInput.startswith(prefix)):
                targetSettings[prefix]=float(infoInput.lstrip(prefix))
                break
    
    return targetSettings

# import my analysis function collection
'''import os, sys, inspect
this_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if this_folder not in sys.path:
  print this_folder
  sys.path.insert(0, this_folder)
import ct_analysis_functions as af
reload(af)'''

def close_all_image_windows():
    # forcefully closes all open images windows
    ids = WindowManager.getIDList();
    if (ids==None):
        return
    for i in ids:
        imp = WindowManager.getImage(i)
        if (imp!=None):
            win = imp.getWindow()
            if (win!=None):
                imp.changes = False # avoids the "save changes" dialog
                win.close()

def mean(x):
    mean = sum(x) / len(x)
    return mean
    
def sd(x):
    mean = sum(x) / len(x)
    differences = [xx - mean for xx in x]
    sq_differences = [xx**2 for xx in differences]
    sd = sqrt(sum(sq_differences)/len(x))
    return sd


def extractChannel(imp, nChannel, nFrame):
    """ Extract a stack for a specific color channel and time frame """
    stack = imp.getImageStack()
    ch = ImageStack(imp.width, imp.height)
    for i in range(1, imp.getNSlices() + 1):
        index = imp.getStackIndex(nChannel, i, nFrame)
        ch.addSlice(str(i), stack.getProcessor(index))
    return ImagePlus("Channel " + str(nChannel), ch)

def measureSumIntensity3D(imp):
    stats = StackStatistics(imp)
    return stats.mean * stats.pixelCount

def autoThreshold(imp, method):
    impout = imp.duplicate() 
    IJ.run(impout, "Auto Threshold", "method=" + method + " white stack use_stack_histogram");
    impout.setTitle("Auto Threshold")
    return impout

def threshold(_img, lower_threshold, upper_threshold):
    imp = Duplicator().run(_img)
    #imp.show(); time.sleep(0.2)
    #IJ.setThreshold(imp, mpar['lthr'], mpar['uthr'])
    IJ.setThreshold(imp, lower_threshold, upper_threshold)
    IJ.run(imp, "Convert to Mask", "stack")
    imp.setTitle("Threshold");
    #IJ.run(imp, "Divide...", "value=255 stack");
    #IJ.setMinAndMax(imp, 0, 1);
    return imp

def compute_overlap(tbModel, iDataSet, impA, iChannelA, iChannelB):
    imp_bw = ImageCalculator().run("AND create stack", impA[iChannelA-1], impA[iChannelB-1])
    overlap_AandB = measureSumIntensity3D(imp_bw)/255
    tbModel.setNumericValue(overlap_AandB, iDataSet, "PBT_"+str(iChannelA)+"AND"+str(iChannelB))
    return tbModel

def getChannelMaxProgection(_originalImage,_channelIndex):
    #extract channel image
    # WARNING: image copy is not created here
    channelImage=extractChannel(_originalImage, _channelIndex, 1)#Duplicator().run(_originalImage,_segmentationChannelIndex,_segmentationChannelIndex,1,imp.getNSlices(),1,1)
    
    #project Stack
    projector=ZProjector()
    projector.setMethod(ZProjector.MAX_METHOD)
    projector.setImage(channelImage)
    projector.doProjection()
    projectedImage=projector.getProjection()
    projectedImage.setTitle('Channel MAX projection')
    
    return projectedImage

def showIterativeGui(_radius, _filterName):
    gd = NonBlockingGenericDialog('Check %s filter output' %_filterName)
    gd.addNumericField('%s filter radius' %_filterName,_radius,0)
    
    gd.setOKLabel("Proceed")
    gd.setCancelLabel("Repeat")
    gd.centerDialog(False)
    gd.setLocation(100, 100) 
    gd.showDialog()
    
    newRadius=int(gd.getNextNumber())
    
    if gd.wasCanceled():
        return True,newRadius
    else:
        return False,newRadius  

def getThickStructuresRoi(_sumProjectionImage):
    
    projectedImage=_sumProjectionImage.duplicate()
    projectedImage.show()
    
    #iteratively define
    rankFilter=RankFilters()
    minMaxRadius=initialMinMaxRadius
    minProcessor=None
    while True:
        minMaxProcessor=projectedImage.getProcessor().duplicate()
        rankFilter.rank(minMaxProcessor,minMaxRadius,RankFilters.MIN)
        minImage=ImagePlus('Minimum filtered image',minMaxProcessor)
        minImage.show()
        repeat, minMaxRadius=showIterativeGui(minMaxRadius,'Minimum')
        #minImage.close()
        if not repeat:
            break
        else:
            minImage.close()
    
    while True:
        minMaxProcessor=minImage.getProcessor().duplicate()
        rankFilter.rank(minMaxProcessor,minMaxRadius,RankFilters.MAX)
        maxImage=ImagePlus('Maximum filtered image',minMaxProcessor)
        maxImage.show()
        repeat, minMaxRadius=showIterativeGui(minMaxRadius,'Maximum')
        maxImage.close()
        if not repeat:
            break
    
    
    projectedImage.close()
    minImage.close()
    
    
    Prefs.blackBackground=False
    minMaxProcessor.autoThreshold()
    minMaxProcessor.invert()
    analysisMask=ImagePlus('Analysis Mask',minMaxProcessor)
    
    
    IJ.setThreshold(analysisMask,0,10)
    exclusionRoi=ThresholdToSelection.run(analysisMask)
    IJ.resetThreshold(analysisMask)
    
    return exclusionRoi

def getManualExclusionRois(_sumProjectionImage):
    _sumProjectionImage.show()
    roiManager=RoiManager.getInstance()
    if roiManager is None:
        roiManager=RoiManager()
    else:
        roiManager.runCommand('Reset')
    
    WaitForUserDialog('Select ROI to exclude from analysis').show()
    manualExclusionRois=roiManager.getRoisAsArray()
    _sumProjectionImage.hide()
    IJ.run(_sumProjectionImage, "Select None", "")
    roiManager.runCommand('Reset')
    return manualExclusionRois

def getManualBackgroundExclusionRoi(_sumProjectionImage,_tissueRadius):
    _sumProjectionImage.show()
    _sumProjectionImage.setRoi(OvalRoi(_sumProjectionImage.getWidth()/2-_tissueRadius,_sumProjectionImage.getHeight()/2-_tissueRadius,2*_tissueRadius,2*_tissueRadius))
    IJ.setTool("oval")
    WaitForUserDialog('Move tissue region to the required position').show()
    _sumProjectionImage.hide()
    IJ.run(_sumProjectionImage, "Make Inverse", "")
    return _sumProjectionImage.getRoi()



# Structure for batch analysis:
#  
# - main 
#  - parameters = get_analysis_parameters()
#  - folder = get_folder()
#  - table = init_results_table()
#  - get_data_info(folder, table) 
#  - batch_analyze(parameters, table) 
#    - for row in table 
#       - analyze(row, table, parameters)
#         - imp = load_imp(table, row)
#         - write results to table(row)
#         - write segmentation overlay images (use from table(row))


def analyze(iDataSet, tbModel, p, output_folder,_nChannels,_imageSettingKeys,_imageSettingKeysShort,_segmentationChannelIndexes, _segmentThickStructures,_manuallyExcludeRegion,_defaultTissueRadius):
    
    #
    # LOAD FILES
    #
    
    filepath = tbModel.getFileAbsolutePathString(iDataSet, "RAW", "IMG")
    filename = tbModel.getFileName(iDataSet, "RAW", "IMG")
    filenametif = '%s.tif' %(os.path.splitext(filename)[0])
    
    print("Analyzing: "+filepath)
    #IJ.run("Bio-Formats Importer", "open=["+filepath+"] color_mode=Default view=Hyperstack stack_order=XYCZT");
    #originalImage = IJ.getImage()
    
    originalImage=BF.openImagePlus(filepath)[0]
    if originalImage.getNChannels()!=_nChannels:
        raise Exception('Incompatible number of channels in the dataset %d' %(iDataset+1))
    #originalImage.show()

    extractedSettings=getImageSettings(originalImage,_imageSettingKeys)
    for settingIndex in range(0,_imageSettingKeys.__len__()):
        tbModel.setNumericValue(extractedSettings[_imageSettingKeys[settingIndex]], iDataSet, _imageSettingKeysShort[settingIndex])
    
    
    #
    # INIT
    #
    
    
    IJ.run("Options...", "iterations=1 count=1"); 
    
    #
    # SHOW DATA
    # 
    
    #originalImage.show()
    
    #
    # SCALING
    #
    
    #IJ.run(originalImage, "Scale...", "x="+str(p["scale"])+" y="+str(p["scale"])+" z=1.0 interpolation=Bilinear average process create"); 
    
    #
    # CONVERSION
    #
    
    #IJ.run(originalImage, "8-bit", "");
    
    #
    # CROPPING
    #
    
    #originalImage.setRoi(392,386,750,762);
    #IJ.run(originalImage, "Crop", "");
    
    
    #
    # BACKGROUND SUBTRACTION
    #
    
    # IJ.run(originalImage, "Subtract...", "value=32768 stack");
    
    #
    # REGION SEGMENTATION
    #
    # general issues:
    # - intensity decreases along z
    
    #get regions to exclude fom analysis
    exclusionOverlay=Overlay()
    segStack=ImageStack(originalImage.getWidth(),originalImage.getHeight())
    for channelIndex in _segmentationChannelIndexes:
        segStack.addSlice(getChannelMaxProgection(originalImage,channelIndex).getProcessor())
    sProjector=ZProjector()
    sProjector.setMethod(ZProjector.SUM_METHOD)
    sProjector.setImage(ImagePlus('Stack of Max projections',segStack))
    sProjector.doProjection()
    sumSegMaxProjectionImage=sProjector.getProjection()
    sumSegMaxProjectionImage.setTitle('Sum of max projections for segmentation channel')
    
    ##excluding thick structures and 
    if _segmentThickStructures:
        structuresExclusionRoi=getThickStructuresRoi(sumSegMaxProjectionImage)
        if structuresExclusionRoi is not None:
            exclusionOverlay.add(structuresExclusionRoi)
    ##excluding manually selected regions
    if _manuallyExcludeRegion:
        manualExclusionRois=getManualExclusionRois(sumSegMaxProjectionImage)
        if manualExclusionRois is not None:
            for mr in manualExclusionRois:
                exclusionOverlay.add(mr)
    ## get circular tissue region
    if _defaultTissueRadius>0:
        manualBackgroundExclusionRoi=getManualBackgroundExclusionRoi(sumSegMaxProjectionImage,_defaultTissueRadius)
        if manualBackgroundExclusionRoi is not None:
            exclusionOverlay.add(manualBackgroundExclusionRoi)

    
    #if any exclusion ROIs detected, save them and clean appropriate regions in the original image
    exclusionRois=exclusionOverlay.toArray()
    if exclusionOverlay.size()>0:
        rm=RoiManager.getInstance()
        rm.runCommand('reset')
        for er in exclusionRois:
            #add to ROIManager to store later on
            rm.addRoi(er)
            #put ROI to the image and clear corresponding region
            originalImage.setRoi(er)
            IJ.run(originalImage, "Clear", "stack")
            
        roiPath=os.path.join(output_folder,'segmented_roi%s.zip' %(os.path.splitext(filename)[0]))
        rm.runCommand('Save',roiPath)
        tbModel.setFileAbsolutePath(roiPath, iDataSet,'Exclusion.Roi', "ROI")
        #reset selection area on the original Image
        IJ.run(originalImage, "Select None", "")
    else:
        print 'Empty exclusion ROI'
    
    
    impA = []
    sumIntensities = []
    sumIntensitiesBW = []
    
    #nChannels = 3
    for iChannel in range(1,_nChannels+1):
        
        # gating
        lower_threshold_value = p["lower_th_ch"+str(iChannel)]
        upper_threshold_value = p["upper_th_ch"+str(iChannel)]
        print("gating channel "+str(iChannel)+" with values from "+str(lower_threshold_value)+" to "+str(upper_threshold_value))
        imp_c = extractChannel(originalImage, iChannel, 1)
        imp_bw = threshold(imp_c, lower_threshold_value, upper_threshold_value) 
        impA.append(imp_bw)  # store the binary mask (0, 255) for OR operations later
        
        # save binary image
        output_filename = "BW_Ch"+str(iChannel)+"--"+filenametif
        IJ.saveAs(imp_bw, "Tiff", os.path.join(output_folder,output_filename))
        tbModel.setFileAbsolutePath(output_folder, output_filename, iDataSet, "BW_"+str(iChannel), "IMG")
         
        # convert from 0,255 to 0,1 for the following calculations
        imp_01 = imp_bw.duplicate()
        IJ.run(imp_01, "Divide...", "value=255 stack"); 
        
        # set all pixels in the intensity image to zero that are not within the gate
        imp_c_gated = ImageCalculator().run("Multiply create stack", imp_c, imp_01)
    
        # save gated image
        output_filename = "GATED_Ch"+str(iChannel)+"--"+filenametif
        IJ.saveAs(imp_c_gated, "Tiff", os.path.join(output_folder,output_filename))
        tbModel.setFileAbsolutePath(output_folder, output_filename, iDataSet, "GATED_"+str(iChannel), "IMG")
        
        # now measure in imp_c_gated and imp_bw(0,1)
        tbModel.setNumericValue(round(measureSumIntensity3D(imp_01),0), iDataSet, "PBT_"+str(iChannel))
        tbModel.setNumericValue(round(measureSumIntensity3D(imp_c_gated),0), iDataSet, "SumIntensity_"+str(iChannel))
        tbModel.setNumericValue(lower_threshold_value, iDataSet, "LOWER_TH_CH"+str(iChannel)) # also record the threshold used
        tbModel.setNumericValue(upper_threshold_value, iDataSet, "UPPER_TH_CH"+str(iChannel)) # also record the threshold used
    
    
    
    # Compute overlaps of binary images
    if nChannels>1:
        indexes=getCombinationsList(_nChannels)
        for f,s in indexes:
            tbModel = compute_overlap(tbModel, iDataSet, impA, f, s)
        #tbModel = compute_overlap(tbModel, iDataSet, impA, 2, 3) 
        #tbModel = compute_overlap(tbModel, iDataSet, impA, 1, 3)
    
    # Compute total volume, i.e. pixels that are above threshold in any of the channels
    if nChannels>2:
        imp_combined = ImageCalculator().run("OR create stack", impA[0], impA[1])
        imp_combined = ImageCalculator().run("OR create stack", imp_combined, impA[2])
        tbModel.setNumericValue(round(measureSumIntensity3D(imp_combined)/255,0), iDataSet, "PBT_1OR2OR3")
    
    
    #impbw = ImageCalculator().run("OR create stack", impbw, impA[2])


#
# ANALYZE INPUT FILES
#
def determine_input_files(foldername, tbModel):
    
    print("Determine input files in:",foldername)
    PBTtern = re.compile('(.*).'+inputFileExtention) 
    
    #PBTtern = re.compile('(.*)--beats.tif') 
    
    i = 0
    for root, directories, filenames in os.walk(foldername):
        for filename in filenames:
            print("Checking:", filename)
            if (filename == "Thumbs.db") or filename.startswith('.'):
                continue
            match = re.search(PBTtern, filename)
            if (match == None) or (match.group(1) == None):
                continue
            tbModel.addRow()
            tbModel.setFileAbsolutePath(foldername, filename, i, "RAW","IMG")
            print("Accepted:", filename)
            
            i += 1
    
    return(tbModel)

#
# GET PARAMETERS
#
def get_parameters(p, keys, num_data_sets, n_channels):
    gd = GenericDialog("Please enter parameters")
    
    gd.addMessage("Found "+str(num_data_sets)+" data sets")
    gd.addStringField("analyse", "all");
    
    gd.addMessage("Image analysis parameters:")
    gd.addMessage("Please note: the threshold values are inclusive!\nThus, to exlude pixels with value 255 the upper threshold needs to be 254")
    
    
    for k in keys:
        gd.addNumericField(k, p[k], 2)
    
    gd.addMessage("Finding target region")
    gd.addStringField('Sum Channels for Segmentation', '%d; %d' %(1,n_channels) if n_channels>1 else '1', 0)
    
    gd.addMessage("Options to exclude regions from quantification")
    
    gd.addCheckbox('Segment and exclude thick structures',True)
    gd.addCheckbox('Manual selection of exclusion region',True)
    gd.addNumericField('Default tissue radius in pixels',0,0)
    
    gd.showDialog()
    if gd.wasCanceled():
        return
    
    to_be_analyzed = gd.getNextString()
    for k in keys:
        p[k] = gd.getNextNumber()
    
    segmentationChannelString=gd.getNextString()
    segmentationChannelIndexes=map(int,segmentationChannelString.split(';'))
    
    segmentThickStructures=gd.getNextBoolean()
    manualExclusionRegion=gd.getNextBoolean()
    selectedRegionRadius = gd.getNextNumber()
    
    return to_be_analyzed, p,segmentationChannelIndexes,segmentThickStructures,manualExclusionRegion, selectedRegionRadius


if __name__ == '__builtin__':

    print("")
    print("#")
    print("# Tissue analysis")
    print("#")
    print("")
    
    #
    # GET INPUT FOLDER
    #
    OpenDialog.setDefaultDirectory(Prefs.get(defDirectoryPrefsTag,None))
    od = OpenDialog("Click on one of the image files in the folder to be analysed", None)
    input_folder = od.getDirectory()
    if input_folder is None:
        sys.exit("No folder selected!")
    Prefs.set(defDirectoryPrefsTag, input_folder)
    
    
    
    #
    # MAKE OUTPUT FOLDER
    #
    output_folder = input_folder[:-1]+"--fiji"
    if not os.path.isdir(output_folder):
        os.mkdir(output_folder)
    
    #
    # DETERMINE INPUT FILES
    #
    tbModel = TableModel(output_folder)
    tbModel.addFileColumns('RAW','IMG')
    tbModel = determine_input_files(input_folder, tbModel)
    
    #
    # CHECK FIRST IMAGE
    #
    filepath = tbModel.getFileAbsolutePathString(0, "RAW", "IMG")
    print("Analyzing: "+filepath)
    #IJ.run("Bio-Formats Importer", "open=["+filepath+"] color_mode=Default view=Hyperstack stack_order=XYCZT");
    #settingsImage = IJ.getImage()
    settingsImage=BF.openImagePlus(filepath)[0]
    #settingsImage.show()
    bit_depth = settingsImage.getBitDepth()
    #settingsImage.close()
    
    
    #
    # GET PARAMETERS
    #
    p = dict()
    keys = list() # necessary to have the correct order in the gui
    nChannels = settingsImage.getNChannels()#3
    for i in range(1,nChannels+1):
        p["lower_th_ch"+str(i)] = 30; keys.append("lower_th_ch"+str(i))
        if bit_depth==8:
            p["upper_th_ch"+str(i)] = 254; keys.append("upper_th_ch"+str(i))
        elif bit_depth==16:
            p["upper_th_ch"+str(i)] = 65534; keys.append("upper_th_ch"+str(i))
        else:
            IJ.log("ERROR: unknown bit depth!")
            sys.exit(0)
    
    to_be_analyzed, p, segmentationChannelIndexes,segmentThickStructures,manuallyExcludeRegion, defaultRegionRadius = get_parameters(p, keys, tbModel.getRowCount(),nChannels)
    print 'Segmentation Channel Indexes', segmentationChannelIndexes
    print 'Segment Thick Structures', segmentThickStructures
    print 'Manually exclude image region from analysis', manuallyExcludeRegion
    print 'Default region radius', defaultRegionRadius
    
    #
    # POPULATE AND SHOW INTERACTIVE TABLE
    #
    
    # thresholds
    for i in range(1, nChannels+1):
        tbModel.addValueColumn("LOWER_TH_CH"+str(i), "NUM")
        tbModel.addValueColumn("UPPER_TH_CH"+str(i), "NUM")
    
    # pixels above threshold (PBT)
    for i in range(1, nChannels+1):
        tbModel.addValueColumn("PBT_"+str(i), "NUM")
    
    # total intensity in whole stack (no masking and no bg-subtraction)
    for i in range(1, nChannels+1):
        tbModel.addValueColumn("SumIntensity_"+str(i), "NUM")
    
    # overlapping PBTs in different channels
    if nChannels>1:
        indexes=getCombinationsList(nChannels)
        for f,s in indexes:
            tbModel.addValueColumn("PBT_%dAND%d" %(f,s), "NUM")
    #tbModel.addValueColumn("PBT_1AND2", "NUM")
    #tbModel.addValueColumn("PBT_2AND3", "NUM")
    if nChannels>2:
        tbModel.addValueColumn("PBT_1OR2OR3", "NUM")
    
    # binary and gated output images
    for i in range(1, nChannels+1):
        tbModel.addFileColumns('BW_'+str(i),'IMG')
        tbModel.addFileColumns('GATED_'+str(i),'IMG')
    
    tbModel.addFileColumns('Exclusion.Roi','ROI')
    
    # add numeric columns for image metadata
    imageSettingKeys,imageSettingKeysShort=getImageSettingKeys(nChannels)
    for setting in imageSettingKeysShort:
        tbModel.addValueColumn(setting,'NUM')
    
    frame=ManualControlFrame(tbModel,False)
    frame.setVisible(True)
    
    #initialise Roi manager
    rm=RoiManager.getInstance()
    if rm is None:
        rm=RoiManager()
    
    #
    # ANALYZE
    #
    
    if not to_be_analyzed=="all":
        close_all_image_windows()
        analyze(int(to_be_analyzed)-1, tbModel, p, output_folder,nChannels,imageSettingKeys,imageSettingKeysShort,
                segmentationChannelIndexes,segmentThickStructures,manuallyExcludeRegion,defaultRegionRadius)
    else:
        for i in range(tbModel.getRowCount()):
            close_all_image_windows()
            analyze(i, tbModel, p, output_folder,nChannels,imageSettingKeys,imageSettingKeysShort,
                    segmentationChannelIndexes,segmentThickStructures,manuallyExcludeRegion,defaultRegionRadius)
    
    close_all_image_windows()
    
    tbModel.writeNewFile('analysis_summary.txt',True)
    
    print 'Analysis finished'