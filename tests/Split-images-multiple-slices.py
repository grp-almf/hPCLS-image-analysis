#required update cites:  IJ-OpenCV, Multi-Template-Matching, AutoMicTools


from ij.gui import Roi,WaitForUserDialog, OvalRoi, NonBlockingGenericDialog
from ij.plugin import ZProjector
from ij.plugin import Duplicator
from ij import IJ, ImagePlus, ImageStack, WindowManager

from ij.process import StackStatistics
from ij.plugin.frame import RoiManager

from fiji.util.gui import GenericDialogPlus
import os.path, re, sys

from loci.plugins import BF


from automic.table import TableModel            # this class stores the data for the table
from automic.table import ManualControlFrame    #this class visualises TableModel via GUI

from automic.parameters import ParameterCollection, ParameterType
from automic.parameters.xml import ParameterXmlWriter
from automic.parameters.gui  import ParameterGuiManager
from automic.parameters.gui  import ParameterPrefsManager

from automic.utils import FileUtils
from automic.utils.roi import ROIManipulator2D

from java.io import File
from java.awt import Font
from java.lang import Integer


prefsPrefixTag='tissue.split.slices'


def validateSliceRegions(_image, _regions):
    _image.show()
    
    IJ.setTool("hand") # "hand" tool seems to be needed for "Ctrl+mouse click" option to select ROIs in the ROI manager by clicking on the image
    
    roiManager=ROIManipulator2D.getEmptyRm()
    for r in _regions:
        roiManager.addRoi(r)
    roiManager.runCommand("UseNames", "true")
    #roiManager.runCommand(_image,"Show All with labels");

    WaitForUserDialog('Manual action required','Check and update slice regions').show()
    _image.hide()
    updatedRegions=roiManager.getRoisAsArray()
    roiManager.runCommand("Reset")
    
    return updatedRegions


def createFileColumnSubfolder(_table, _tag, _type):
    _table.addFileColumns(_tag,_type)
    FileUtils.createCleanFolder(_table.getRootPath(),_tag)

def createImageTableModel( _analysisPath):

    tbModel = TableModel(_analysisPath)

    tbModel.addFileColumns('OriginalImage','IMG')
    createFileColumnSubfolder(tbModel, 'Region','ROI')
    createFileColumnSubfolder(tbModel, 'CropImage','IMG')

    tbModel.addValueColumn('CropZMin','NUM')
    tbModel.addValueColumn('CropZMax','NUM')

    return tbModel



def saveImageTable(_image,_fileName, _table, _iDataset, _tag):
    fullPath= os.path.join(_table.getRootPath(),_tag,_fileName)
    print 'Fullcrop path '+fullPath
    IJ.saveAs(_image, "Tiff", fullPath)
    _table.setFileAbsolutePath(fullPath, _iDataset, _tag, "IMG")



def close_all_image_windows():

    # forcefully closes all open images windows
    ids = WindowManager.getIDList();
    if (ids==None):
        return

    for i in ids:
        imp = WindowManager.getImage(i)
        if (imp!=None):
            win = imp.getWindow()
            if (win!=None):
                imp.changes = False # avoids the "save changes" dialog
                win.close()





def extractChannel(imp, nChannel, nFrame):

    """ Extract a stack for a specific color channel and time frame """
    stack = imp.getImageStack()
    ch = ImageStack(imp.width, imp.height)
    for i in range(1, imp.getNSlices() + 1):
        index = imp.getStackIndex(nChannel, i, nFrame)
        ch.addSlice(str(i), stack.getProcessor(index))

    return ImagePlus("Channel " + str(nChannel), ch)





def getChannelMaxProgection(_originalImage,_channelIndex):
    #extract channel image
    # WARNING: image copy is not created here
    channelImage=extractChannel(_originalImage, _channelIndex, 1)#Duplicator().run(_originalImage,_segmentationChannelIndex,_segmentationChannelIndex,1,imp.getNSlices(),1,1)
    
    #project Stack
    projector=ZProjector()
    projector.setMethod(ZProjector.MAX_METHOD)
    projector.setImage(channelImage)
    projector.doProjection()
    projectedImage=projector.getProjection()
    projectedImage.setTitle('Channel MAX projection')
    
    return projectedImage


def findSliceRegions(_micImage,_patternSize,_nSlices):
    _micImage.setTitle("MicImage")
    IJ.run(_micImage, "Select None", "");


    #create pattern image
    patternImage = IJ.createImage("PatternImage", "8-bit black", _patternSize, _patternSize, 1)
    patternImage.setRoi(OvalRoi(0,0,_patternSize,_patternSize))
    IJ.setForegroundColor(255, 255, 255);
    IJ.run(patternImage, "Fill", "slice");
    IJ.run(patternImage, "Select None", "");

    rm=ROIManipulator2D.getEmptyRm()
    
    patternImage.show()
    _micImage.show()
    IJ.run(_micImage, "Template Matching Image", "template=PatternImage image=MicImage rotate=[] matching_method=[Normalised 0-mean cross-correlation] number_of_objects=%d score_threshold=0 maximal_overlap=0.1 add_roi" %_nSlices)

    patternImage.hide()
    _micImage.hide()
    
    regions=rm.getRoisAsArray()
    
    return regions

def getZRange(_minimumZIndex,_maximumZIndex):
    stackDialog=NonBlockingGenericDialog('Specify stack range')
    stackDialog.addNumericField("First slice index: ", _minimumZIndex, 0)
    stackDialog.addNumericField("Last slice index: ", _maximumZIndex, 0)
    stackDialog.showDialog()
    if stackDialog.wasCanceled():
        return _minimumZIndex,_maximumZIndex
    newMinimumZIndex = (int)(stackDialog.getNextNumber())
    newMaximumZIndex = (int)(stackDialog.getNextNumber())
    
    return newMinimumZIndex, newMaximumZIndex



def splitImageFiles(_imageFile, _tbModel, _processingParameters):
    
    
    print("Processing: "+_imageFile)

    basename=os.path.splitext(os.path.basename(_imageFile))[0]


    originalImage=BF.openImagePlus(_imageFile)[0]

    projectedImage=getChannelMaxProgection(originalImage,_processingParameters.getParameterValue("Channel Index"))
    
    #projectedImage.show()


    sliceRegions=findSliceRegions(projectedImage,_processingParameters.getParameterValue("Slice Diameter Pixels"),_processingParameters.getParameterValue("Number of Tissue Slices"))

    sliceRegions=validateSliceRegions(projectedImage,sliceRegions)
    sliceIndex=0
    minimumZIndex=1
    maximumZIndex=originalImage.getNSlices()
    for sliceRegion in sliceRegions:
        #remove stack information from ROI properties
        sliceRegion.setPosition(0,0,0)
        
        sliceIndex=sliceIndex+1
        originalImage.setRoi(sliceRegion)
        sliceImage=Duplicator().run(originalImage)
        if _processingParameters.getParameterValue("Select substack"):
            sliceImage.show()
            minimumZIndex,maximumZIndex=getZRange(minimumZIndex,maximumZIndex)
            sliceImage.hide()
            sliceImage=Duplicator().run(sliceImage,1,sliceImage.getNChannels(),minimumZIndex,maximumZIndex,1,sliceImage.getNFrames())
        
        
        _tbModel.addRow()
        iDataSet=_tbModel.getRowCount()-1
        cropFileName='%s--%03d.tif' %(basename,sliceIndex)
        
        saveImageTable(sliceImage,cropFileName, _tbModel, iDataSet, 'CropImage')
        
        _tbModel.setFileAbsolutePath(_imageFile, iDataSet, 'OriginalImage', "IMG")
        
        #saveSliceRoi
        regionFileName='%s--%03d.zip' %(basename,sliceIndex)
        regionFileNameWoExt='%s--%03d' %(basename,sliceIndex)
        regionFileLocation=os.path.join(_tbModel.getRootPath(),'Region')
        _tbModel.setFileAbsolutePath(regionFileLocation,regionFileName, iDataSet, 'Region', "ROI")
        ROIManipulator2D.saveRoiToFile(regionFileLocation,regionFileNameWoExt,sliceRegion)

        #save stacj Information
        _tbModel.setNumericValue(minimumZIndex,iDataSet, 'CropZMin')
        _tbModel.setNumericValue(maximumZIndex,iDataSet, 'CropZMax')
        
    return


#
# FIND INPUT FILES
#
def determine_input_files(foldername, _extension):

    imageFiles=[]
    
    print("Determine input files in:",foldername)

    PBTtern = re.compile('(.*).'+_extension) 

    

    for root, directories, filenames in os.walk(foldername):

        for filename in filenames:
            print("Checking:", filename)
            match = re.search(PBTtern, filename)
            if (match == None) or (match.group(1) == None):
                continue
            imageFiles.append(os.path.join(foldername,filename))
            
    
    return imageFiles


#

# GET PARAMETERS

#

def get_parameters():
    
    inputCollection=ParameterCollection()
    inputCollection.addParameter("Input Data Path",'',None,ParameterType.FOLDERPATH_PARAMETER)
    inputCollection.addParameter("Input File Extension",'czi',None,ParameterType.FOLDERPATH_PARAMETER)
    inputCollection.addParameter("Slice Diameter Pixels",3800,None,ParameterType.INT_PARAMETER)
    inputCollection.addParameter("Channel Index",2,None,ParameterType.INT_PARAMETER)
    inputCollection.addParameter("Number of Tissue Slices",3,None,ParameterType.INT_PARAMETER)
    inputCollection.addParameter("Select substack",False,None,ParameterType.BOOL_PARAMETER)

    ParameterPrefsManager.setParametersFromFijiPrefs(inputCollection, prefsPrefixTag)

    startupDialog=ParameterGuiManager(inputCollection)
    
    if not startupDialog.refineParametersViaDialog("Slice splitting parameters"):
        IJ.showMessage("Processing was aborted by user.");
        return None

    ParameterPrefsManager.putParametersToFijiPrefs(inputCollection, prefsPrefixTag)

    
    
    return inputCollection



if __name__ in ('__main__','__builtin__'):
    
    #
    # GET ANALYSIS PARAMETERS
    #
    
    analysisParameters=get_parameters()
    

    if analysisParameters is None:
        sys.exit()
    
    #
    # MAKE OUTPUT FOLDER
    #
    input_folder = analysisParameters.getParameterValue("Input Data Path")
    output_folder = input_folder.rstrip('\\/')+"--split"

    FileUtils.createCleanFolder(File(output_folder))
    

    # WRITE ANALYSIS PARAMETERS TO THE FILE
    ParameterXmlWriter().writeParametersToFile(analysisParameters,File(output_folder,"analysis_parameters.xml"))
    
    #

    # DETERMINE INPUT FILES

    #

    tbModel = createImageTableModel(_analysisPath = output_folder)
    inputFiles = determine_input_files(input_folder,analysisParameters.getParameterValue("Input File Extension"))
    
    print "Input files: %s" %inputFiles
    
    frame=ManualControlFrame(tbModel,False)

    frame.setVisible(True)

    

    #initialise Roi manager
    rm=RoiManager.getInstance()
    if rm is None:
        rm=RoiManager()

    tbModel.writeNewFile('slice_splitting_summary.txt',True)
    for inputFile in inputFiles:
        splitImageFiles(inputFile,tbModel,analysisParameters)
        tbModel.writeNewFile('slice_splitting_summary.txt',True)
    
    
    #close_all_image_windows()
    
    
    print 'Processing finished'