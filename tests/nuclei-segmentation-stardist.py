#@ CommandService command

from automic.table import TableModel            # this class stores the data for the table

from automic.table import ManualControlFrame    #this class visualises TableModel via GUI

from automic.parameters import ParameterCollection, ParameterType
from automic.parameters.xml import ParameterXmlWriter
from automic.utils import FileUtils

from java.awt import Font, Color
from java.io import File
from loci.plugins import BF


from ij.plugin import Duplicator, ZProjector
from fiji.util.gui import GenericDialogPlus
from ij import IJ, ImagePlus, Prefs, WindowManager
from ij.plugin.frame import RoiManager

from net.imglib2.img.display.imagej import ImageJFunctions

import re,os


from de.csbdresden.stardist import StarDist2D

from automic.utils.roi import ROIManipulator2D



defDirectoryPrefsTag='nuclei.analysis.default.directory'


def close_all_image_windows():
    # forcefully closes all open images windows
    ids = WindowManager.getIDList();
    if (ids==None):
        return

    for i in ids:
        imp = WindowManager.getImage(i)
        if (imp!=None):
            win = imp.getWindow()
            if (win!=None):
                imp.changes = False # avoids the "save changes" dialog
                win.close()

def createFileColumnSubfolder(_table, _tag, _type):
    _table.addFileColumns(_tag,_type)
    FileUtils.createCleanFolder(_table.getRootPath(),_tag)

def createImageTableModel( _analysisPath):

    tbModel = TableModel(_analysisPath)

    tbModel.addColumn('DatasetId')
    tbModel.addFileColumns('RAW','IMG')
    createFileColumnSubfolder(tbModel,'Nuclei.Mask','IMG')
    createFileColumnSubfolder(tbModel,'Nuclei.Rois','ROI')

    tbModel.addValueColumn("Nuclei.Center", "NUM")
    tbModel.addValueColumn("Nuclei.Border", "NUM")
    tbModel.addValueColumn("Nuclei.Total", "NUM")
    tbModel.addValueColumn("Nuclei.Weighted", "NUM")
    
    return tbModel


def saveImageTable(_image,_fileName, _table, _iDataset, _tag):
    fullPath= os.path.join(_table.getRootPath(),_tag,_fileName)
    IJ.saveAs(_image, "Tiff", fullPath)
    _table.setFileAbsolutePath(fullPath, _iDataset, _tag, "IMG")

def isRoiOnBorder(_roi, _image):
    
    bounds=_roi.getBounds()
    if bounds.x<=0:
        return True
    
    if bounds.y<=0:
        return True
    
    if (bounds.x+bounds.width)>=_image.getWidth():
        return True
    
    if (bounds.y+bounds.height)>=_image.getHeight():
        return True
    
    return False

def analyze(iDataSet, tbModel, _analysisParameters):

    rm=ROIManipulator2D.getEmptyRm()

    #

    # LOAD FILES

    #

    

    filepath = tbModel.getFileAbsolutePathString(iDataSet, "RAW", "IMG")

    filename = tbModel.getFileName(iDataSet, "RAW", "IMG")
    basename=os.path.splitext(filename)[0]
    filenametif = '%s.tif' %(basename)
    
    print("Analyzing: "+filepath)


    originalImage=BF.openImagePlus(filepath)[0]
    channelIndex=_analysisParameters.getParameterValue("Nuclei Channel")
    if originalImage.getNChannels()>1:
        originalImage=Duplicator().run(originalImage,channelIndex,channelIndex,1,originalImage.getNSlices(),1,1)

    if originalImage.getNSlices()>1:
        mProjector=ZProjector()
        mProjector.setMethod(ZProjector.MAX_METHOD)
        mProjector.setImage(originalImage)
        mProjector.doProjection()
        originalImage=mProjector.getProjection()
    
    IJ.run("Options...", "iterations=1 count=1"); 

    res = command.run(StarDist2D, False,
            "input",originalImage,
            "modelChoice", "Versatile (fluorescent nuclei)",
            ).get()
    label = res.getOutput("label").getImgPlus()
    label1=ImageJFunctions.wrap(label,"Label")


    saveImageTable(_image=label1,_fileName=filenametif, _table=tbModel, _iDataset=iDataSet, _tag='Nuclei.Mask')
    
    nucleiRois=rm.getRoisAsArray()

    if nucleiRois is None:
        nNuclei=0
    else:
        nNuclei=len(nucleiRois)

    tbModel.setNumericValue(nNuclei, iDataSet, "Nuclei.Total")
    
    if False:
        tbModel.setNumericValue(0, iDataSet, "Nuclei.Center")
        tbModel.setNumericValue(0, iDataSet, "Nuclei.Border")
        tbModel.setNumericValue(0, iDataSet, "Nuclei.Weighted")
        return

    nucleiCenter=0
    nucleiBorder=0
    for r in nucleiRois:
        if isRoiOnBorder(_roi=r, _image=originalImage):
            r.setStrokeColor(Color.PINK)
            nucleiBorder+=1
        else:
            r.setStrokeColor(Color.GREEN)
            nucleiCenter+=1

    tbModel.setNumericValue(nucleiCenter, iDataSet, "Nuclei.Center")
    tbModel.setNumericValue(nucleiBorder, iDataSet, "Nuclei.Border")
    tbModel.setNumericValue(nucleiCenter+nucleiBorder/2.0, iDataSet, "Nuclei.Weighted")

    nucleiLocation = os.path.join(tbModel.getRootPath(),'Nuclei.Rois')
    ROIManipulator2D.saveRoisToFile(nucleiLocation,basename,nucleiRois)
    tbModel.setFileAbsolutePath(nucleiLocation,'%s.zip' %basename,iDataSet,'Nuclei.Rois','ROI')


#

# ANALYZE INPUT FILES

#

def determine_input_files(foldername, tbModel, _regExpression):

    

    print("Determine input files in:",foldername)
    print _regExpression
    PBTtern = re.compile(_regExpression) 

    i = 0

    for root, directories, filenames in os.walk(foldername):

        for filename in filenames:
            print("Checking:", filename)
            match = re.search(PBTtern, filename)
            if match is None:
                continue
            tbModel.addRow()
            tbModel.setFileAbsolutePath(foldername, filename, i, "RAW","IMG")
            tbModel.setStringValue(filename, i,'DatasetId')
            print("Accepted:", filename)
       
            i += 1

    return tbModel

#

# GET PARAMETERS

#

def get_parameters():
    
    inputCollection=ParameterCollection()
    
    
    headerFont=Font(Font.SERIF,Font.BOLD,14)
    
    gd = GenericDialogPlus("Enter analysis settings")
    gd.addMessage("Input Data", headerFont)
    
    
    gd.addStringField("Datasets to Analyse", "all");
    gd.addStringField("Input File Regular Expression", ".*\)\.tif");
    gd.addNumericField("Nuclei Channel",1,0)
    gd.addDirectoryField("Input Data Path", Prefs.get(defDirectoryPrefsTag,None),40)
    
    
    gd.showDialog()
    if gd.wasCanceled():
        WaitForUserDialog("Analysis aborted by User").show()
        sys.exit("Analysis aborted by User")
    
    inputCollection.addParameter("Datasets to Analyse",gd.getNextString(),None,ParameterType.STRING_PARAMETER)
    inputCollection.addParameter("Input File Regular Expression",gd.getNextString(),None,ParameterType.STRING_PARAMETER)
    inputCollection.addParameter("Nuclei Channel",int(gd.getNextNumber()),None,ParameterType.INT_PARAMETER)
    inputCollection.addParameter("Input Data Path",gd.getNextString(),None,ParameterType.FOLDERPATH_PARAMETER)
    Prefs.set(defDirectoryPrefsTag, inputCollection.getParameterValue("Input Data Path"))
    
    return inputCollection


if __name__ == '__builtin__':



    analysisParameters=get_parameters()

    input_folder = analysisParameters.getParameterValue("Input Data Path")
    output_folder = input_folder.rstrip('\\/')+"--fiji-Nuc"

    FileUtils.createCleanFolder(File(output_folder))
    

    # WRITE ANALYSIS PARAMETERS TO THE FILE
    #ParameterXmlWriter().writeParametersToFile(analysisParameters,File(output_folder,"analysis_parameters.xml"))
    
    #

    # DETERMINE INPUT FILES

    #

    tbModel = createImageTableModel(_analysisPath = output_folder)
    tbModel = determine_input_files(input_folder, tbModel,analysisParameters.getParameterValue("Input File Regular Expression"))

    frame=ManualControlFrame(tbModel,False)

    frame.setVisible(True)
    
    #initialise Roi manager
    rm=RoiManager.getInstance()
    if rm is None:
        rm=RoiManager()
    
    #

    # ANALYZE

    #

    to_be_analyzed = analysisParameters.getParameterValue("Datasets to Analyse")
    if not to_be_analyzed=="all":
        close_all_image_windows()
        analyze(int(to_be_analyzed)-1, tbModel, analysisParameters)

    else:
        for i in range(tbModel.getRowCount()):
            close_all_image_windows()
            analyze(i, tbModel, analysisParameters)
    
    close_all_image_windows()
    
    tbModel.writeNewFile('analysis_summary_image_nuc.txt',True)
    
    print 'Analysis of nuclei finished'