from ij import IJ
from ij.gui import OvalRoi
from automic.utils.roi import ROIManipulator2D


patternSize=3900
nSlices=3

micImage=IJ.getImage()

micImage.setTitle("MicImage")


#create pattern image
patternImage = IJ.createImage("PatternImage", "8-bit black", patternSize, patternSize, 1)
patternImage.show()
patternImage.setRoi(OvalRoi(0,0,patternSize,patternSize))
IJ.setForegroundColor(255, 255, 255);
IJ.run(patternImage, "Fill", "slice");
IJ.run(patternImage, "Select None", "");

rm=ROIManipulator2D.getEmptyRm()

IJ.run(micImage, "Template Matching Image", "template=PatternImage image=MicImage rotate=[] matching_method=[Normalised 0-mean cross-correlation] number_of_objects=%d score_threshold=0 maximal_overlap=0.1 add_roi" %nSlices)

patternImage.hide()
