# Discussions


## 2019-10-22

Muzmil and Aliaksandr discussed preparing script for publication. Activities are tracked [issue 7](grp-almf/hPCLS-image-analysis#7).

## 2018-07-30

Muzamil and Aliaksandr discussed new features to reduce data heterogeneity. New features to implement are summarised in [issue 3](grp-almf/hPCLS-image-analysis#3).

## 2017-07-19

Muzamil and Tischi discussed the project.

We found that the intensity in all channels strongly decreases (~20% per 10um slice) if one gets deeper into the tissue.

As the tissue is not flat on the coverslip and is also thicker than the z-range that Muzamil typically imaged this could lead to artefacts in the quantification.

We thus discussed to somehow identify the lowest z-planes in the tissue for each tile and only quantify those, such that for each image tile only the (bright) beginning of the tissue would be imaged.

The general issue however is that there currently is no clean way to decide whether a certain plane in one of the tile stacks is already in the tissue or not.

The other problem is that for some tiles already the lowest z-plane is in the tissue that means we miss the brightest planes.

=> one probably would need an algorithm that only takes the lowest "tissue" z-planes in tiles where the first planes are still outside the tissue.

Ideas:
- define a pixel as part of the tissue if it is closer than X um (in xy!) to the next autofluoresent pixel above threshold.
    - this does not work in z because the tissue physically abruptly starts and ends
- in the stitched images, manually select ROIs in ImageJ that are in the tissue and not an airway
    - Image > Stacks > Plot Z-Axis Profile
        - This gives the mean intensity as a function of depth
        - One could for instance take highest value of these plots as a readout, arguing that this is the first planes that is fully in the tissue
        - Planes that are deeper are affected by the scattering of the light by the tissue
        - Planes that are lower are not fully in the tissue
        - NOTE: this will work best if the tissue is flat on the coverslip, if it is very tilted one would have to find this "lowest plane in the tissue" tile by tile

After 3 hours of discussion we decided to stop for today and continue another day
