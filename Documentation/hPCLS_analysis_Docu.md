# Selective analysis of interstitial collagen in hPCLS

## General information

This Jython script is dedicated for processing 3D multichannel images of high precision cut lung slices (hPCLS) in order to quantify collagen deposition. Script performs either fully automated or semi-automated analysis of multiple image datasets:
- For each individual input channel 
	- Creates binary masks for z-stacks by applying user-specified thresholds => **BW** images.
	- Applies created masks on the original image stacks and converts background pixles to 0 => **GATED** images  
	- Measures the number of object pixels **BW** images (as measure of deposited collagen volume) and sum of intensities of **GATED** images (as measure of the amount of deposited collagen).
- Creates masks for overlaps and combinations of masks of different channels => **PBT** images.
  Quantifies number of positive (object) pixels on these images. 
- Creates 2D projection of collagen deposition by summing up max projections of specified channels (e.g. forward and backward harmonic), in which representative images are acquired.
- Creates a grid of regions on the input image and performs all measurements listed above in the individual regions. Optionally, allows user to interactively delete regions from the analysis.
- Saves all produced images
- Saves all quantifications and links to generated image files in a tabular format. Data can be interactively browsed using cababilities of AutoMicTools library (see [Browsing of analysis results](#browsing-of-analysis-results) section below).

## Installation

- **Install Fiji** if you did not have installed it before.
- **Install AutomicTools library**. Dowhload `AutoMic_JavaTools-xxx.jar` file from [here](https://git.embl.de/halavaty/AutoMicTools/-/releases) and copy it to the plugins subdirectory of your Fiji installation. Fiji needs to be restarted afterwards.

  > Note: `hPCLS-microscopy-image-analysis.py` was tested with AutoMicTools version  1.1.4 released on January, 3 2019, but it is recommended to use the latest available version of AutoMicTools.  

- **Download `hPCLS-microscopy-image-analysis.py` script** to your computer and open it in Fiji either by *Drag-And-Drop* into Fiji status bar or via main dialog (*File*->*Open*).

## Input data format

Folder with image files to be analysed. Input image files have to satisfy the following requirements:
- Images should be compatible with Bio-Formats ImageJ plugin, because the script  uses Bio-Formats for image opening.
- Each file has to contain one dataset (images from single stage position).
- Images for Z-slices and channels of one dataset have to be in the same file.
- Time-lapse data can be analysed, but images for different timepoints have to be in different files.
- All image files in the same input folder should have the same format:
	- Bit depth
	- Number of z-slices
	- Number and order of channels
- If tile scans are acquired to cover complete slice area, stitching has to be performed before starting the script.

## Output data format

Output folder, containing:
- Text files with result tables
- Subfolders with generated images

Output folder is created automatically under the same path where input folder is located. Name of the output folder has format *< name-of-input-folder >--fiji*

  > Note: If such folder already exists, script will automatically erase all its content before analysis is started. Rename folder with the results of the previous analysis if you want to keep these data along with the results of new analysis. 

Text files contain tab-separated tabular data with references to input and generated images, as well as quantifications. There are two output tables:
- *analysis_summary_image.txt*: ane line corresponds to one dataset (image). Numbers in the numeric columns correspond to the measurements for for the whole z-tack (PCLS).
- *analysis_summary_regions.txt*: One line corresponds to one region in the grid pattern. Numbers are for each ROI/PCLS. This table has all the columns that are present in image table and references to the quantified regions. These references allow visualise quantified regions with the ***AutoMic Browser*** tool after analysis (see [Browsing of analysis results](#browsing-of-analysis-results) section below).

Names of the numeric columns in both files have format:
- ***PBT.x_NUM***: Number of the threshold pixels in channel x
- ***SumIntensity.x_NUM***:Sum intensity of the gated image in channel x
- ***PBT.xANDy_NUM***: Number of pixels common between respective channels
- ***PBT.1OR2OR3_NUM***: Number of pixels positive in either of three channels (multiple pixels positive in different channels only counted once).

Generated images are stored in the appropriate subfolders  with the names of the files corresponding to the names of the input images:
- ***BW.x***: Binary masks of channel x. 
- ***GATED.x***: Gated stack for channel X
- ***Second.Harmonic.Max.Proj*** folder contains processed image which is a sum of max projections of the channels defined in the analysis parameters


## Workflow

### Data processing

1. Open the script in FIJI
2. Press **Run** button in the script window to start the analysis
3. Specify analysis parameters:                                                  
	- **Datasets to analyse**: either ***all***	(default) or index of the dataset to be analysed (for testing).
	- **Input File Extension**: only image files with specified extension will be analysed.
	- **Number of Channels**: only images with specified number of channels will be analysed.
	- **Input Data Path**: path to the input folder with images to process.
	- **Filter grid regions manually**: select this option for interactive selection of the grid regions which have to be excluded from the analysis
	- **Region Split X** and **Region split Y**: the dimensions of ROIs grid that the images are divided into for the region analysis (e.g. 3X3 or more).
4. Press **OK** to continue

5. Specify channel-specific parameters in the second dialog:
	- The threshold values for signal in each channel (the values can be estimated before running the script manually or via the test run of the script)
	- **Sum of Channels for Segmentation**: the indexes of the channels for which max need to be combined for defining analysis regions.
6. Press **OK** to start the analysis. Follow the analysis progress in the console panel of the script window. At the same time AutoMic Browser tool will open automatically with the image-based table. Measured values will appear in the table during the analysis.
7. If **Filter grid regions manually** option was selected, projected images will pop up automatically superexposed by the defined number of ROIs.
Delete the ROIS that are not required from the ROI manager (removed regions will not appear in the region-based table).
For selecting discarded regions directly from the image use the following steps (more info [here](https://forum.image.sc/t/activate-roi-from-roi-manager-clicking-on-the-selections-instead-of-its-label/683))
	- "Hand" tool in Fiji has to be activated (grid script activates in automatically).
	- **Ctrl+ mouse click** (**Cmd + mouse click** for Mac) on the image selects corresponding ROI in RoiManager. Pressing **Del** will remove corresponding ROI from RoiManager (multiple regions can not be selected this way).
	- If region selected by mistake, another region can not be selected. Can press **Ctrl++Shift+A** to deselect all regions, then new selection is possible.

8. When the analysis is completed, the *Analysis finished* message will appear in the console panel.


### Browsing of analysis results

Generated text files can be opened in either spreadsheet software (e.g. MS Excel) or statistical packages (e.g. R) and subjected to analysis as per the user’s needs.

Alternatively, these tables can be browsed with the special Fiji plugin ***AutoMic Browser***, which allows easily navigate through the datasets and correlate measurements to corresponding images and ROIs:
-  *AutoMicTools* package in Fiji needs to be installed (see [Installation](#Installation)).
-  Start ***AutoMic Browser***: *Plugins*->*Auto Mic Tools* -> *AutoMic Browser*
-  Select one of generated test files in the dialog.
-  Specify which images have to be visualised by ticking appropriate checkboxes in the bottom panel.
-  Recommended optional visualisation parameters (bottom left panel):
	-  Select **Fit image to the frame** to appropriately scale big images into corresponding windows
	-  Select ***Min_max*** option in the **Image Contrast** drop box to adjust contrast automatically when another image is opened.
-  Tick **Region.Roi** checkboxes under appropriate image headers to outline region that corresponds to the selected dataset (only for region-based table). 
-  Click on different lines of the table to navigate through the datasets. Content of image windows will be updated accordingly.

![](./images/automic-browser.JPG)

The **AutoMic Browser** tool can be used to quality control analysis results:
- In the table panel select line with the measurements that have have to be excluded from the downstream analysis.
- Unselect **Use dataset** checkbox. Corresponding value in the last column of the table(*Success_BOOL*) will changle from ***true*** to ***false***.
- Press **Save** button to save modified table (new table is always save in the same folder as the original table, therefore only file name needs to be specified).  